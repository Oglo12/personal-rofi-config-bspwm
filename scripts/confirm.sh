#!/bin/bash

yes="Yes, I am sure."
no="No! Abort!"

answer=$(echo -e "$yes\n$no" | rofi -i -dmenu -p "Are you sure?:")

if [[ $answer == $yes ]]; then
	echo "true"
else
	echo "false"
fi
